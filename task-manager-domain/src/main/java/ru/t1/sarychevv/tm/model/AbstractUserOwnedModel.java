package ru.t1.sarychevv.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    @Column(nullable = false)
    protected String name = "";
    @NotNull
    @Column(nullable = false)
    protected String description = "";
    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}

