package ru.t1.sarychevv.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.response.AbstractResponse;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
