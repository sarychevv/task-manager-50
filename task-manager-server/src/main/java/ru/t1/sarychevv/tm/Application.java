package ru.t1.sarychevv.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
