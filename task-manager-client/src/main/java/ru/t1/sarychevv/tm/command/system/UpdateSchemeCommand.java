package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.dto.request.system.UpdateSchemeRequest;
import ru.t1.sarychevv.tm.dto.response.system.UpdateSchemeResponse;

public class UpdateSchemeCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update scheme.";
    }

    @Override
    @NotNull
    public String getName() {
        return "update";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE SCHEME]");
        @NotNull final UpdateSchemeResponse response = getServiceLocator().
                getSystemEndpoint().updateScheme(new UpdateSchemeRequest(getToken()));
        System.out.println(response);

    }
}
