package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.response.project.ProjectClearResponse;
import ru.t1.sarychevv.tm.dto.response.project.ProjectGetByIndexResponse;
import ru.t1.sarychevv.tm.dto.response.project.ProjectUpdateByIndexResponse;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.marker.SoapCategory;

import java.util.List;

public class ProjectEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("admin");
        userLoginRequest.setPassword("admin");
        token = authEndpoint.login(userLoginRequest).getToken();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
            projectCreateRequest.setName("Project-" + i);
            projectCreateRequest.setDescription("Description-" + i);
            projectEndpoint.createProject(projectCreateRequest);
        }

    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusById() {
        @NotNull ProjectChangeStatusByIdRequest projectChangeStatusByIdRequest = new ProjectChangeStatusByIdRequest(token);
        @NotNull ProjectGetByIndexRequest projectGetByIndexRequest = new ProjectGetByIndexRequest(token);
        projectGetByIndexRequest.setIndex(1);
        @NotNull ProjectGetByIndexResponse projectGetByIndexResponse = projectEndpoint.getProjectByIndex(projectGetByIndexRequest);
        @NotNull String Id = projectGetByIndexResponse.getProject().getId();
        projectChangeStatusByIdRequest.setStatus(Status.COMPLETED);
        projectChangeStatusByIdRequest.setId(Id);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest).getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByIndex() {
        @NotNull ProjectChangeStatusByIndexRequest projectChangeStatusByIndexRequest = new ProjectChangeStatusByIndexRequest(token);
        projectChangeStatusByIndexRequest.setStatus(Status.COMPLETED);
        projectChangeStatusByIndexRequest.setIndex(2);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest).getProject().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateProject() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project");
        projectCreateRequest.setDescription("Description");
        projectEndpoint.createProject(projectCreateRequest);
        @NotNull final ProjectGetByIndexRequest projectGetByIndexRequest = new ProjectGetByIndexRequest(token);
        projectGetByIndexRequest.setIndex(NUMBER_OF_ENTRIES);
        Assert.assertEquals("Project", projectEndpoint.getProjectByIndex(projectGetByIndexRequest).getProject().getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectById() {
        @NotNull ProjectGetByIdRequest projectGetByIdRequest = new ProjectGetByIdRequest(token);
        @NotNull ProjectGetByIndexRequest projectGetByIndexRequest = new ProjectGetByIndexRequest(token);
        projectGetByIndexRequest.setIndex(1);
        @NotNull ProjectGetByIndexResponse projectGetByIndexResponse = projectEndpoint.getProjectByIndex(projectGetByIndexRequest);
        @NotNull String Id = projectGetByIndexResponse.getProject().getId();
        projectGetByIdRequest.setId(Id);
        Assert.assertNotNull(projectEndpoint.getProjectById(projectGetByIdRequest));
    }

    @Test
    @Category(SoapCategory.class)
    public void testListProject() {
        @NotNull final ProjectListRequest projectListRequest = new ProjectListRequest(token);
        Assert.assertNotNull(projectEndpoint.listProject(projectListRequest));
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndex() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project-1");
        projectCreateRequest.setDescription("Description-1");
        projectEndpoint.createProject(projectCreateRequest);
        @NotNull final ProjectRemoveByIndexRequest projectRemoveByIndexRequest = new ProjectRemoveByIndexRequest(token);
        projectRemoveByIndexRequest.setIndex(NUMBER_OF_ENTRIES);
        Assert.assertEquals("Project-1", projectEndpoint.removeProjectByIndex(projectRemoveByIndexRequest).getProject().getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndex() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project-2");
        projectCreateRequest.setDescription("Description-2");
        projectEndpoint.createProject(projectCreateRequest);
        @NotNull final ProjectUpdateByIndexRequest projectUpdateByIndexRequest = new ProjectUpdateByIndexRequest(token);
        projectUpdateByIndexRequest.setName("Project test update name");
        projectUpdateByIndexRequest.setDescription("Project test update description");
        projectUpdateByIndexRequest.setIndex(NUMBER_OF_ENTRIES);
        @NotNull final ProjectUpdateByIndexResponse projectUpdateByIndexResponse = projectEndpoint.updateProjectByIndex(projectUpdateByIndexRequest);
        Assert.assertEquals("Project test update name", projectUpdateByIndexResponse.getProject().getName());
        Assert.assertEquals("Project test update description", projectUpdateByIndexResponse.getProject().getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearProjects() {
        ProjectClearResponse projectClearResponse = projectEndpoint.clearProject(new ProjectClearRequest(token));
        @Nullable List<ProjectDTO> projectList = null;
        @NotNull ProjectListRequest projectListRequest = new ProjectListRequest(token);
        Assert.assertEquals(projectList, projectEndpoint.listProject(projectListRequest));
    }

}
